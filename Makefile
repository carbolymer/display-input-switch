SHELL := /bin/bash

include variables.mk

.PHONY: help install

build: ##@Build the application
	mkdir -p out/
	./chores build
	cp `cabal list-bin disw` out/

install: ##@Install Install the application
	install -o root -g root -m 0755 -D out/disw /usr/bin/
	useradd -s /bin/nologin -g i2c i2c || true
	groupadd -f i2c
	install -o root -g root -m 0644 -D extra/30-i2c-dev.conf /etc/modules-load.d/
	install -o root -g root -m 0644 -D extra/10-local_i2c_group.rules /etc/udev/rules.d/
	install -o root -g root -m 0644 -D extra/10-allow-reboot-i2c.rules /etc/polkit-1/rules.d/
	install -o root -g root -m 0644 -D extra/disw.service /etc/systemd/system/
	install -b -o root -g root -m 0644 -D extra/disw.yaml /etc/disw/disw.yaml
	install -o root -g root -m 0644 -D html/* /usr/share/disw/html/
	systemctl daemon-reload
