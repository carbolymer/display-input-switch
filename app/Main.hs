{-# OPTIONS_GHC -Wno-type-defaults #-}
module Main where

import Adapters.Ddc
import Adapters.Http
import Adapters.SystemdBoot
import Application.Configuration
import Application.Middleware
import Application.Monad
import Application.Feature
import Blammo.Logging
import Control.Monad.IO.Unlift
import Core.Commands
import Options.Applicative
import Protolude
import Ports.Bootloader

main :: IO ()
main = execParser cmdOptions >>= startServer
  where
    cmdOptions = info (cmdArgsP <**> helper)
        ( fullDesc
          <> progDesc "DISW"
          <> header "Display Input SWitcher" )

cmdArgsP :: Parser CmdArgs
cmdArgsP = CmdArgs
  <$> option str
    ( short 'c'
      <> long "config"
      <> help "config file path"
      <> value defaultConfigLocation)
  <*> switch
    ( long "dry-run"
      <> help "use stubbed ddc (do not call ddc)" )

data CmdArgs = CmdArgs
  { configFile :: FilePath
  , dryRun     :: Bool
  }

startServer :: CmdArgs -> IO ()
startServer CmdArgs{configFile, dryRun} = do
  config <- loadConfiguration configFile
  let displayPresets = displayPresetsById config
      (monitorControl, bootloader) =
        if dryRun then (newStubbedMonitorControl, newStubbedSystemdBoot)
                  else (newMonitorControl, newSystemdBoot)
      commands = newCommands
      features = mempty
  runApp Env{..} $ do
    -- feature check is delayed until we built app
    features' <- enableWhen bootloaderFeature (checkIfSupported bootloader) features
    local (\e -> e{features=features'}) $ do
      waiSettings <- makeWaiSettings
      logInfo $ "Starting disw" :# ["dryRun" .= dryRun, "host" .= host config, "port" .= port config]
      logInfo $ "Available features" :# ["features" .= features']
      withRunInIO $ \runInIO -> do
        waiApp <- scottyApp $ diswApi (liftIO . runInIO)
        runSettings config waiSettings waiApp
