{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE RankNTypes                #-}
module Main where

import           Data.Conduit.Shell  hiding (info)
import           Options.Applicative
import           Protolude
import           System.Environment  (setEnv)

main :: IO ()
main = execParser opts >>= run . runCmd
  where
    opts = info (cmdOptionsP <**> helper)
      ( fullDesc
      <> progDesc "Task runner"
      <> header "chores - a simple task runner" )

data Chore
  = Build { isFast :: Bool }
  | Clean
  -- | Housekeeping -- TODO: weeder + prune-juice w/ installation
  | Run

cmdOptionsP :: Parser Chore
cmdOptionsP = hsubparser . mconcat $
  [ command "build" (info buildP (progDesc "build"))
  , command "clean" (info (pure Clean) (progDesc "clean"))
  , command "run" (info (pure Run) (progDesc "run just built app in dry run mode"))
  ]
    where
      buildP :: Parser Chore
      buildP = Build <$> flag False True (long "fast" <> help "build fast")

runCmd :: Chore -> Segment IO ()
runCmd (Build False) = cabal "build" "-O2" "-j"
runCmd (Build True)  = cabal "build" "-O0" "-fobject-code" "-j"
runCmd Clean         = cabal "clean"
runCmd Run           = do
  liftIO $ setEnv "LOG_LEVEL" "debug"
  cabal "run" "disw" "-v0" "--" "-c" "./extra/disw.yaml" -- "--dry-run"
