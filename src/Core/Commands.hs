module Core.Commands where

import Application.Configuration
import Application.Feature
import Application.Monad
import Blammo.Logging
import Data.Map.Strict           qualified as M
import Ports.Bootloader          as Bootloader
import Ports.Commands
import Ports.MonitorControl
import Protolude.Extended

newCommands ::
  ( MonadApp r m
  , Has Configuration r
  , Has (MonitorControl m) r
  , Has (Bootloader m) r
  , Has Features r
  , Has DisplayPresetsById r
  ) => Commands m
newCommands = Commands
  { getBootloaderEntries = ifM (hasFeature bootloaderFeature) getBootloaderEntries' (pure [])
  , rebootInto = \bId -> ifM (hasFeature bootloaderFeature) (rebootInto' bId) (pure False)
  , ..
  }
  where
    getDisplayPresets = do
      presets <- grabs presets
      logDebug $ "Retrieving presets" :# ["presets" .= presets]
      pure presets

    setPreset dpId@DisplayPresetId{unDisplayPresetId=presetId} = do
      presets :: DisplayPresetsById <- grab
      case M.lookup dpId presets of
        Nothing -> pure . Left $ "Unknown preset:" <> presetId
        Just preset -> do
          logDebug $ "Trying to set" :# ["preset" .= preset]
          MonitorControl{setDisplayInputs} <- grab
          setDisplayInputs preset
          logDebug $ "Set" :# ["preset" .= preset]
          purer ()

    getBootloaderEntries' = do
      Bootloader{getAllEntries} <- grab
      getAllEntries

    rebootInto' bId = do
      Bootloader{getAllEntries, rebootInto = rebootInto''} <- grab
      handleError $ do
        selectedEntry <- filter (\BootloaderEntry{id} -> id == bId) <$> getAllEntries
        if length selectedEntry /= 1
           then do
             logWarn $ "Requested entry not found!" :# ["entryId" .= bId]
             pure False
           else do
             rebootInto'' bId
             pure True
      where
        handleError act = catch act $ \(e :: BootloaderError) -> do
          logWarn $ "Cannot reboot" :# ["error" .= (show e :: Text)]
          pure False
