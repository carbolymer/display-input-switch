module Adapters.Ddc where

import           Application.Configuration
import           Blammo.Logging
import           Ports.MonitorControl
import           Protolude
import           System.Process            (callProcess)
import           Text.Printf               (printf)

newMonitorControl :: (HasCallStack, MonadIO m, MonadLogger m) => MonitorControl m
newMonitorControl = MonitorControl{..}
  where
    checkIfHasCapability = liftIO $ do
      result :: Either SomeException () <- try $ callProcess "/bin/ddcutil" ["detect"]
      pure $ isRight result

    setDisplayInputs DisplayPreset{displayInputs} = do
      for_ displayInputs $ \DisplayInput{displayNumber, displayInputSource} -> do
        liftIO $ callProcess "ddcutil" ["setvcp", "60", printf "0x%x" displayInputSource, "-d", show displayNumber]

    getAllDisplayInputs = undefined

newStubbedMonitorControl :: (HasCallStack, MonadLogger m) => MonitorControl m
newStubbedMonitorControl = MonitorControl{..}
  where
    checkIfHasCapability = logDebug "DdcStub: checkIfHasCapability" >> pure True
    setDisplayInputs dp = logDebug $ "DdcStub: setDisplayPreset" :# ["preset" .= dp]
    getAllDisplayInputs = logDebug "DdcStub: getAllDisplayInputs"
