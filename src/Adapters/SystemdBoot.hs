module Adapters.SystemdBoot where

import Blammo.Logging
import Control.Monad.Trans.Extended
import Control.Monad.Trans.Maybe
import Data.Aeson                   qualified as A
import Data.ByteString              qualified as B
import Data.ByteString.Lazy.Char8   qualified as BC
import Data.Text                    qualified as T
import Data.Text.Encoding           qualified as T
import Ports.Bootloader
import System.FilePath              ((</>))
import Protolude.Extended
import System.Process
import UnliftIO.Directory           (listDirectory)

newSystemdBoot :: forall m. (HasCallStack, MonadUnliftIO m, MonadLogger m, MonadCatch m) => Bootloader m
newSystemdBoot = Bootloader{..}
  where
    -- check if current bootloader is systemd
    checkIfSupported = do
      efivars <- listDirectory efiDir
      result <- handleException . runMaybeT $ do
        varName <- hoist . head $ filter (isPrefixOf "LoaderInfo") efivars
        efivar <- liftIO $ B.readFile (efiDir </> varName)
        -- drop efivar attributes https://www.kernel.org/doc/html/latest/filesystems/efivarfs.html
        let efivarData = T.decodeUtf16LE $ B.drop 4 efivar
        pure $ T.isPrefixOf "systemd-boot" efivarData
      pure $ result == Just True
        where
          efiDir = "/sys/firmware/efi/efivars"
          handleException act = catch act $ \(e :: SomeException) -> do
            logDebug $ "Cannot detect systemd bootloader in " <> toS efiDir :# ["exception" .= (show e :: Text)]
            pure Nothing

    getAllEntries = do
      entries <- handleWrap @SomeException (RetrievingEntriesError . show) . liftIO $
        BC.pack <$> readProcess "bootctl" ["list", "--json=pretty"] ""
      A.eitherDecode entries `throwEitherWith` (DecodingEntriesError . toS)

    rebootInto bId =
      handleWrap @SomeException (SelectingNextBootError . show) . liftIO $
        callProcess "systemctl" ["reboot", "-i", "--boot-loader-entry", toS bId]

newStubbedSystemdBoot :: (HasCallStack, MonadLogger m) => Bootloader m
newStubbedSystemdBoot = Bootloader{..}
  where
    checkIfSupported = logDebug "SystemdBootStub: checkIfSupported" >> pure True
    getAllEntries = logDebug "SystemdBootStub: getAllEntries" >> pure []
    rebootInto bid = logDebug $ "SystemdBootStub: rebootInto" :# ["boot id" .= bid]
