{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Adapters.Http where


import Application.Configuration
import Application.Monad
import Blammo.Logging
import Data.Aeson                (ToJSON)
import Data.Text.Lazy            qualified as TL
import Network.Mime              (defaultMimeLookup)
import Ports.Commands
import Protolude                 hiding (get)
import Web.Scotty                (ActionM, ScottyM, addHeader, file, get, json, param, post)

diswApi :: HasCallStack => (forall a. App a -> ActionM a) -> ScottyM ()
diswApi runApp' = do
  get "/api/health" $
    json @Text "OK"

  get "/api/presets" $
    json <=< runApp' . join $ grabs getDisplayPresets

  post "/api/preset/:id" $ do
    presetId <- param "id"
    json <=< runApp' $ do
      Commands{setPreset} <- grab
      setPreset presetId <&> \case
        Left err -> SwitchResult False (Just err)
        Right () -> SwitchResult True Nothing

  get "/api/boot/entries" $
    json <=< runApp' . join $ grabs getBootloaderEntries

  post "/api/boot/reboot/:id" $ do
    entryId <- param "id"
    json <=< runApp' $ do
      Commands{rebootInto} <- grab
      rebootInto entryId

  get "/" $ do
    loadFile "index.html"

  get "/:file" $ do
    param "file" >>= loadFile

  where
    loadFile fileName = do
      addHeader "Content-Type" . toS $ defaultMimeLookup fileName
      runApp' . logDebug  $ "Request file" :# ["file" .= fileName]
      file $ "html/" <> toS fileName

data SwitchResult = SwitchResult
  { success :: !Bool
  , message :: !(Maybe Text)
  } deriving (Generic, ToJSON)


instance ConvertText ByteString TL.Text where
  -- partial!
  toS = TL.fromStrict . decodeUtf8
