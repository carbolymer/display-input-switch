module Protolude.Extended
  ( module Protolude
  , module Control.Exception.Safe
  , module Protolude.Extended
  , module UnliftIO
  ) where

import Control.Exception.Safe
import Protolude              hiding (Handler, bracket, bracketOnError, bracket_, catch, catchJust, catches, finally,
                               handle, handleJust, mask, mask_, onException, throwIO, throwTo, try, tryIO, tryJust,
                               uninterruptibleMask, uninterruptibleMask_)
import UnliftIO               (MonadUnliftIO)


throwEitherWith :: (HasCallStack, MonadThrow m, Exception e) => Either b a -> (b -> e) -> m a
throwEitherWith res f = withFrozenCallStack $ case res of
  Right a -> pure a
  Left b -> throw $ f b

handleWrap :: forall e1 m e2 a. (HasCallStack, MonadCatch m, Exception e1, Exception e2) => (e1 -> e2) -> m a -> m a
handleWrap f act = withFrozenCallStack $ catch act (throw . f)

