{-# LANGUAGE DeriveAnyClass #-}
module Application.Configuration where

import Data.Aeson               (FromJSON)
import Data.Aeson.Types         (ToJSON)
import Data.Map.Strict          qualified as M
import Data.Yaml                (decodeFileThrow)
import Network.Wai.Handler.Warp (Port)
import Protolude
import Web.Scotty               (Parsable)


data Configuration = Configuration
  { host    :: !Text
  , port    :: !Port
  , tls     :: !(Maybe TlsConfiguration)
  , presets :: ![DisplayPreset]
  } deriving (Generic, ToJSON, FromJSON)

data DisplayPreset = DisplayPreset
  { id            :: !DisplayPresetId
  , presetName    :: !Text
  , displayInputs :: ![DisplayInput]
  } deriving (Generic, ToJSON, FromJSON)

newtype DisplayPresetId = DisplayPresetId { unDisplayPresetId :: Text }
  deriving (Generic, Ord, Eq)
  deriving newtype (IsString, FromJSON, ToJSON, Parsable)

data DisplayInput = DisplayInput
  { displayNumber      :: !Int
  , displayInputSource :: !Int
  } deriving (Generic, ToJSON, FromJSON)

data TlsConfiguration = TlsConfiguration
  { certFile :: !FilePath
  , keyFile  :: !FilePath
  } deriving (Generic, ToJSON, FromJSON)

defaultConfigLocation :: FilePath
defaultConfigLocation = "/etc/disw/config.yaml"

loadConfiguration :: (HasCallStack, MonadIO m) => FilePath -> m Configuration
loadConfiguration = decodeFileThrow

type ErrorMessage = Text
type DisplayPresetsById = M.Map DisplayPresetId DisplayPreset

displayPresetsById :: Configuration -> M.Map DisplayPresetId DisplayPreset
displayPresetsById cfg = M.fromList $ map (\dp@DisplayPreset{id} -> (id, dp)) (presets cfg)

-- {{{ Data access class
class Has a b where
  obtain :: b -> a

grab :: (MonadReader r m, Has a r) => m a
grab = asks obtain

grabs :: (MonadReader r m, Has a r) => (a -> b) -> m b
grabs f = asks (f . obtain)

-- }}}

