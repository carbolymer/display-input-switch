module Application.Middleware where

import Application.Configuration
import Application.Monad
import Blammo.Logging
import Control.Monad.IO.Unlift           (MonadUnliftIO, withRunInIO)
import Data.String                       (fromString)
import Data.Text                         qualified as T
import Network.Wai                       qualified as Wai
import Network.Wai.Handler.Warp          (defaultSettings, defaultShouldDisplayException, setHost, setOnException,
                                          setPort)
import Network.Wai.Handler.Warp          qualified as Warp
import Network.Wai.Handler.Warp.Internal (Settings (..))
import Network.Wai.Handler.WarpTLS       (WarpTLSException (..), runTLS, tlsSettings)
import Protolude
import Web.Scotty                        qualified as Scotty

scottyApp :: Scotty.ScottyM () -> IO Wai.Application
scottyApp = Scotty.scottyApp

runSettings :: Configuration -> Settings -> Wai.Application -> IO ()
runSettings Configuration{tls} = case tls of
  Just TlsConfiguration{certFile, keyFile} -> runTLS (tlsSettings certFile keyFile)
  Nothing                                  -> Warp.runSettings

makeWaiSettings :: (MonadApp r m, Has Configuration r, MonadUnliftIO m) => m Settings
makeWaiSettings = do
  Configuration{..} <- grab
  withRunInIO $ \runInIO -> do
    pure $ defaultSettings
      & setPort port
      & setHost (fromString $ T.unpack host)
      & setOnException (onEx runInIO)
      where
        onEx runInIO _req ex =
          when (shouldDisplayException ex)
            . runInIO
            . logError
            $ "Warp exception"
            :# ["exception" .= displayException ex]

        shouldDisplayException ex
          | Just InsecureConnectionDenied <- fromException @WarpTLSException ex = False
          | otherwise = defaultShouldDisplayException ex
