module Application.Feature where

import Data.Aeson         (ToJSON)
import Data.Set           qualified as S
import Protolude.Extended hiding (show, typeRep)

newtype Feature = Feature Text
  deriving (Ord, Eq)
  deriving newtype (IsString, Show, ToJSON)

type Features = Set Feature

enableWhen :: Monad m => Feature -> m Bool -> Features -> m Features
enableWhen f en fs = ifM en (pure $ S.insert f fs) (pure fs)
