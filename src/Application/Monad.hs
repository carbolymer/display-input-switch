{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE TypeFamilies #-}
module Application.Monad where

import Application.Configuration
import Blammo.Logging            (LoggingT, MonadLogger)
import Blammo.Logging.Simple     (runSimpleLoggingT)
import Control.Applicative
import Control.Exception.Safe
import Control.Monad
import Ports.Commands            (Commands)
import Ports.MonitorControl      (MonitorControl)
import Protolude
import UnliftIO                  (MonadUnliftIO)
import Ports.Bootloader
import Application.Feature
import Data.Set qualified as S

newtype App a = AppT { unAppT :: ReaderT AppEnv (LoggingT IO) a }
  deriving newtype (Functor, Applicative, Monad, MonadReader AppEnv, MonadLogger, MonadIO, MonadUnliftIO, MonadThrow, MonadCatch)

runApp :: (HasCallStack, MonadIO m) => AppEnv -> App a -> m a
runApp env = liftIO . runSimpleLoggingT . flip runReaderT env . unAppT

type AppEnv = Env App
data Env m = Env
  { config         :: !Configuration
  , displayPresets :: !DisplayPresetsById
  , monitorControl :: !(MonitorControl m)
  , commands       :: !(Commands m)
  , bootloader     :: !(Bootloader m)
  , features       :: !Features
  }

instance Has Configuration (Env m) where obtain = config
instance Has DisplayPresetsById (Env m) where obtain = displayPresets
instance Has (MonitorControl m) (Env m) where obtain = monitorControl
instance Has (Commands m) (Env m) where obtain = commands
instance Has (Bootloader m) (Env m) where obtain = bootloader
instance Has Features (Env m) where obtain = features

type MonadApp r m = (HasCallStack, MonadReader r m, MonadLogger m, MonadCatch m)

hasFeature :: (MonadApp r m, Has Features r) => Feature -> m Bool
hasFeature f = grabs (S.member f)

