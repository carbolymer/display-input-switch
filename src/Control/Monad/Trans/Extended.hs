module Control.Monad.Trans.Extended where

import Protolude

import Control.Monad.Trans.Maybe

class MonadHoist m t where
  hoist :: m a -> t a

instance Monoid e => MonadHoist Maybe (Either e) where
  hoist = \case
    Nothing -> Left mempty
    Just a -> Right a

instance MonadHoist (Either e) Maybe where
  hoist = \case
    Left _ -> Nothing
    Right a -> Just a

instance Applicative m => MonadHoist Maybe (MaybeT m) where
  hoist = MaybeT . pure

instance Applicative m => MonadHoist (Either e) (MaybeT m) where
  hoist = hoist . \case
    Left _ -> Nothing
    Right a -> Just a

instance (Monoid e, Applicative m) => MonadHoist Maybe (ExceptT e m) where
  hoist = ExceptT . pure . \case
    Nothing -> Left mempty
    Just a -> Right a

instance Applicative m => MonadHoist (Either e) (ExceptT e m) where
  hoist = ExceptT . pure
