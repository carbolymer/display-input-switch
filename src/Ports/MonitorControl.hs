module Ports.MonitorControl where

import           Application.Configuration
import           Protolude

data MonitorControl m = MonitorControl
  { checkIfHasCapability :: HasCallStack => m Bool
  , setDisplayInputs     :: HasCallStack => DisplayPreset -> m ()
  , getAllDisplayInputs  :: HasCallStack => m ()
  }

