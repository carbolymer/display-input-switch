module Ports.Commands where

import Application.Configuration
import Ports.Bootloader
import Protolude

data Commands m = Commands
  { getDisplayPresets    :: m [DisplayPreset]
  , setPreset            :: DisplayPresetId -> m (Either ErrorMessage ())
  , getBootloaderEntries :: m [BootloaderEntry]
  , rebootInto           :: BootloaderEntryId -> m Bool
  }
