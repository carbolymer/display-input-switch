{-# LANGUAGE DeriveAnyClass #-}
module Ports.Bootloader where

import Application.Feature
import Data.Aeson          (FromJSON, ToJSON)
import Protolude.Extended
import Web.Scotty          (Parsable)
import Data.String (String)

bootloaderFeature :: Feature
bootloaderFeature = "bootloader"

data Bootloader m = Bootloader
  { checkIfSupported :: HasCallStack => m Bool
  , getAllEntries    :: (HasCallStack, MonadCatch m) => m [BootloaderEntry]
  , rebootInto       :: (HasCallStack, MonadCatch m) => BootloaderEntryId -> m ()
  }

data BootloaderEntry = BootloaderEntry
  { id         :: BootloaderEntryId
  , title      :: Text
  , isDefault  :: Bool
  , isSelected :: Bool
  } deriving (Show, Generic, FromJSON, ToJSON)


newtype BootloaderEntryId = BootloaderEntryId Text
  deriving (Ord, Eq)
  deriving newtype (IsString, Show, ToJSON, FromJSON, Parsable)

instance ConvertText BootloaderEntryId String where
  toS (BootloaderEntryId t) = toS t

data BootloaderError
  = RetrievingEntriesError !Text
  | DecodingEntriesError !Text
  | SelectingNextBootError !Text
  deriving (Show, Exception)


