{
  description = "display input switch";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, utils, ... }:
  utils.lib.eachDefaultSystem (system:
  let
    config = { };

    overlay = pkgsNew: pkgsOld: {
      haskellPackages = pkgsOld.haskell.packages.ghc94.override (old: {
        overrides = pkgsNew.haskell.lib.packageSourceOverrides {
          disw = ./.;
        };
      });
    };

    pkgs = import nixpkgs { inherit config system; overlays = [ overlay ]; };
    
    disw = with pkgs.haskell.lib.compose; pkgs.lib.trivial.pipe pkgs.haskellPackages.disw [
      (setBuildTargets ["disw-test" "exe:disw"])
      justStaticExecutables
      dontHaddock
      # static compilation - requires musl
      # (appendConfigureFlags [
      #   "--enable-executable-static" 
      #   "--enable-executable-stripping"
      #   "--disable-shared"
      #   "--ghc-option=-optl=-static"
      #   "--extra-lib-dirs=${pkgs.gmp.override { withStatic = true; }}/lib"
      #   "--extra-lib-dirs=${pkgs.zlib.static}/lib"
      #   "--extra-lib-dirs=${pkgs.libffi.overrideAttrs (old: { dontDisableStatic = true; })}/lib"
      #   "--extra-lib-dirs=${pkgs.ncurses.override { enableStatic = true; }}/lib"
      # ])
    ];
  in
  {
    packages.default = disw;

    apps.default = {
      type = "app";
      program = "${disw}/bin/disw";
    };

    apps.myScript = utils.lib.mkApp { drv = pkgs.writeShellScriptBin "my-script" ''
      #!/usr/bin/env bash
      echo "Hello from my script!"
      ghc --version
      '';
    };

    devShells.default = pkgs.haskellPackages.disw.env;
  });
}
