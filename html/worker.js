const cacheName = 'disw-v1'
const assets = [
  '/',
  '/manifest.webmanifest',
  '/index.html',
  '/app.js',
  '/style.css',
  '/icon.svg',
  '/icon64.png',
  '/icon128.png',
  '/icon192.png',
  '/icon512.png',
  '/api/presets'
]

self.addEventListener('install', installEvent => {
  installEvent.waitUntil(
    caches.open(cacheName)
      .then(cache => {
        // cache expected assets
        cache.addAll(assets)
      })
  )
})

self.addEventListener('fetch', event => {
  const path = new URL(event.request.url).pathname

  // skip urls not handled as assets
  if(!assets.includes(path)) {
    return
  }
  event.respondWith(
    (async () => {
      // Try to get the response from a cache.
      const cache = await caches.open(cacheName)
      const cachedResponse = await cache.match(event.request)

      if (cachedResponse) {
        // If we found a match in the cache, return it, but also
        // update the entry in the cache in the background.
        event.waitUntil(cache.add(event.request))
        return cachedResponse
      }

      // If we didn't find a match in the cache, use the network.
      return fetch(event.request)
    })()
  )
})
