// {{{ Data access

async function getAllPresets() {
  const res = await fetch('/api/presets')
  return await res.json()
}

async function selectPreset(presetName) {
  const response = await fetch('/api/preset/' + presetName, {method: 'POST'})
  console.log(await response.json())
}

async function getBootEntries() {
  const res = await fetch('/api/boot/entries')
  return await res.json()
}

async function rebootInto(bootEntryId) {
  const response = await fetch('/api/boot/reboot/' + bootEntryId, {method: 'POST'})
  console.log(await response.json())
}

async function checkHealth() {
  const res = await fetch('/api/health')
  return await res.json()
}

// }}}

// {{{ Online check

async function setPresetIfOnline() {
  const setSelected = async function() {
    const selectPresetId = localStorage.getItem('selectPreset')
    if(!!selectPresetId) {
      localStorage.removeItem('selectPreset')
      await selectPreset(selectPresetId)
    }
    renderOnlineCheck(true)
  }
  checkHealth()
    .then(setSelected)
    .catch(_ => renderOnlineCheck(false))
}

// }}}

//{{{ Rendering

function renderPresets(presets) {
  const presetsDiv = document.getElementById('presets-buttons')
  presetsDiv.innerHTML = presets.map(preset =>
    `<button onclick="localStorage.setItem('selectPreset', '${preset.id}')">${preset.presetName}</button>`
  ).join('')
}

function renderBootOptions(bootOptions) {
  const bootOptionsDiv = document.getElementById('boot-buttons')
  bootOptionsDiv.innerHTML = bootOptions.map(option =>
    `<button onclick="rebootInto('${option.id}')">${option.title}</button>`
  ).join('')
}

function renderOnlineCheck(isOnline) {
  const onlineCheckDiv = document.getElementById('online-check')
  onlineCheckDiv.innerHTML = isOnline
    ? '<span class="green">ONLINE <img class="logo" src="icon.svg"/></span>'
    : '<span class="red">OFFLINE <img class="logo" src="icon.svg"/></span>'
}

//}}}

async function init() {
  console.debug('fetching initial configuration')
  setInterval(setPresetIfOnline, 1000)
  try {
    let presets = await getAllPresets()
    let bootOptions = await getBootEntries()
    renderPresets(presets)
    renderBootOptions(bootOptions)
  } catch (e) {
    console.error(e)
  }
}

//{{{ Service worker registration
window.addEventListener('load', _ => {
  let initSequence
  if ('serviceWorker' in navigator) {
      initSequence = navigator.serviceWorker
        .register('/worker.js')
        .then(_ => console.log('service worker registered'))
        .catch(err => console.log('service worker not registered', err))
  } else {
    console.warn('no service workers available')
    initSequence = Promise.resolve(null)
  }
  initSequence.then(init)
})
//}}}

